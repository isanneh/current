class CreateTracks < ActiveRecord::Migration[5.2]
  def change
    create_table :tracks do |t|

      t.string :name
      t.string :artist
      t.string :link
      t.belongs_to :user, index: true
      t.timestamps
    end
  end
end
