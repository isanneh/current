class UsersController < ApplicationController
  def signup
    if current_user
      return redirect_to '/'
    end
    @title = 'Signup'
    session['error'] = nil 
    @user = User.new
  end

  def create
    session['error'] = nil
    if User.find_by_username(user_params[:username])
        session['error'] = 'Username already exists!'
        @user = User.new
        render 'signup'
    else
      @user = User.new(user_params)
      if @user.save
        session[:username] = @user.username
        redirect_url = '/tracks/' + @user.username
        redirect_to redirect_url
      else
        session['error'] = 'Sorry, you were not able to create an account'
        render 'signup' 
      end
    end
  end

  def login
    if current_user
      return redirect_to '/'
    end
    @title = 'Login'
    session['error'] = nil
  end
  
  def authenticate
    session['error'] = nil
    @user = User.find_by_username(params[:user][:username])
    if @user && @user.authenticate(params[:user][:password])
      session[:username] = @user.username
      redirect_url = '/tracks/' + @user.username
      redirect_to redirect_url
    else
      session['error'] = 'Incorrect username or password'
      render 'login'
    end
  end

  def logout
    session['error'] = nil
    session[:username] = nil
    redirect_to '/'
  end

  private
    def user_params
      params.require(:user).permit(:username, :password)
  end

end
