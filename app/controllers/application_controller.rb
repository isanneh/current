class ApplicationController < ActionController::Base

  helper_method :current_user 

  def current_user 
    @current_user ||= User.find_by_username(session[:username]) if session[:username] 
  end

  def login_required 
    redirect_to '/login' unless current_user 
  end

end
