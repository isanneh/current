class TracksController < ApplicationController
  before_action :login_required, only: [:add_track, :create]

  def index
    @title = 'Top Playlists'
    session['error'] = nil
    @top_playlists = User.joins(:tracks).group('users.id')
  end

  def playlist
    session['error'] = nil
    @tracks = Track.all
  end

  def add_track
    @title = 'Add New Track'
    session['error'] = nil
    @track = current_user.tracks.new
  end

  def show
    @title = params[:username] + "'s " + 'Playlist'
    session['error'] = nil
    @user = User.find_by_username(params[:username])
    if @user
      @tracks = @user.tracks
    else
      @tracks = []
      session['error'] = 'User does not exist!'
    end
  end

  def create
    session['error'] = nil
    @track = current_user.tracks.new(message_params)
    if @track.save
      redirect_url = '/tracks/' + current_user.username
      redirect_to redirect_url
    else
      render 'add_track'
    end
  end

  private 
    def message_params 
      params.require(:track).permit(:name, :artist, :link)
  end

end
