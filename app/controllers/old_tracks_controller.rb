class TracksController < ApplicationController
  before_action :login_required, only: [:add_track, :create]

  def index
  end
  def playlist
    @tracks = Track.all
  end
  def add_track
    puts('user')
    puts(current_user.username)
    #@track = Track.new
    @track = current_user.tracks.new
  end
  def create
    #@track = Track.new(message_params)
    @track = current_user.tracks.new(message_params)
    puts(@current_user)
    #@track.user = @current_user
    if @track.save
      redirect_to '/tracks'
    else
      render 'add_track'
    end
  end
  private 
    def message_params 
      params.require(:track).permit(:name, :artist, :link)#.merge(user: current_user)
  end
end
