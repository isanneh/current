Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '' => 'tracks#index'
  get 'tracks/:username' => 'tracks#show', as: :playlist
  get 'add-track' => 'tracks#add_track'
  post 'add-track' => 'tracks#create'
  get 'signup' => 'users#signup'
  post 'signup' => 'users#create'
  get 'login' => 'users#login'
  post 'login' => 'users#authenticate'
  delete 'logout' => 'users#logout'
end
